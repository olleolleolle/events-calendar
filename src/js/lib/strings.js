const strings = {
  'en': {
    'princeIdPresent': 'The Prince of Insulae Draconis will be present',
    'princessIdPresent': 'The Princess of Insulae Draconis will be present',
    'princeAndPrincessIdPresent': 'The Prince and Princess of Insulae Draconis will be present',

    'princeNordmarkPresent': 'The Prince of Nordmark will be present',
    'princessNordmarkPresent': 'The Princess of Nordmark will be present',
    'princeAndPrincessNordmarkPresent': 'The Prince and Princess of Nordmark will be present',

    'kingPresent': 'The King will be present',
    'queenPresent': 'The Queen will be present',
    'kingAndQueenPresent': 'The King and Queen will be present',
    'royaltyPresent': 'Royalty Present',

    'heavy': 'Armoured Combat',
    'heavyScheduled': 'Armoured Combat is scheduled',
    'fencing': 'Fencing',
    'fencingScheduled': 'Fencing is scheduled',
    'archery': 'Archery',
    'archeryScheduled': 'Archery is scheduled',
    'artsci': 'Arts and Sciences',
    'artsciScheduled': 'Arts and Sciences activities are scheduled',
    'dancing': 'Dancing',
    'dancingScheduled': 'Dancing is scheduled',
    'online': 'Takes place online',
    'importantInfo': 'Important Info',
    'cancelled': 'Cancelled',
    'cancelledLong': 'This event has been cancelled',
    'contactStaff': 'Please contact the event staff for further information.',

    'bidList': 'Bid List',
    'addEvent': 'Add an event to the calendar',
    'pastEvents': 'Past Events',

    'hideLocal': 'Hide local/non-official events',
    'showLocal': 'Show local/non-official events',

    'hideCancelled': 'Hide cancelled events',
    'showCancelled': 'Show cancelled events',

    'eventsApprovedByChronicler': 'Events which have been approved by the Chronicler as official have a white background.',
    'upcomingEvents': 'Upcoming Events',

    // Columns of the calendar

    'date': 'Date',
    'group': 'Group',
    'event': 'Event',
    'info': 'Info',

    // When loading, this shows "Loading calendar attempt 1", "Loading calendar attempt 2", and so on
    'loadingAttempt': 'Loading calendar attempt',
    'loadFailed': 'Calendar failed to load',

    'eventNotFound': 'This event was not found in the calendar.',

    // Strings for the event pages

    'hostedBy': 'Hosted by',
    'viewMap': 'View on Google Maps',
    'siteAddress': 'Site address',
    'siteInfo': 'Site information',
    'cost': 'Pricing',
    'reservation': 'Reservation',
    'payment': 'Payment',
    'joinOnline': 'Join the online meeting',
    'pwInfo': 'Password information',
    'eventSteward': 'Event steward',
    'visitWebsite': 'Visit the event website',
    'visitFB': 'Follow this event on Facebook',

    'chronOfficial': 'This event has been accepted for publication by the Kingdom Chronicler',
    'chronUpdated': 'This event has been updated since being accepted for publication by the Chronicler',
    'chronPending': 'This event is pending review by the Chronicler',
    'chronUnofficial': 'The Kingdom Chronicler requires more information before accepting this event for publication',
    'chronLocal': 'This local event is not submitted for publication in the Kingdom Newsletter',
    'chronOnline': 'This is an online-only event',

    // Info pages

    'appliesTo': 'Applies to',
    'moreInfo': 'More Information...',
    'informational': 'Informational',

  },

  'sv': {
    'princeIdPresent': 'Fursten av Insulae Draconis kommer närvara',
    'princessIdPresent': 'Furstinnan av Insulae Draconis kommern närvara',
    'princeAndPrincessIdPresent': 'Fursteparet av Insulae Draconis kommer närvara',

    'princeNordmarkPresent': 'Fursten av Nordmark kommer närvara',
    'princessNordmarkPresent': 'Furstinnan av Nordmark kommer närvara',
    'princeAndPrincessNordmarkPresent': 'Fursteparet av Nordmark kommer närvara',

    'kingPresent': 'Kungen kommer närvara',
    'queenPresent': 'Drottningen kommer närvara',
    'kingAndQueenPresent': 'Kungaparet kommer närvara',
    'royaltyPresent': 'Kungligheter utöver Kungaparet kommer närvara',

    'heavy': 'Kämpalek',
    'heavyScheduled': 'Kämpalek är schemalagt',
    'fencing': 'Fäktning',
    'fencingScheduled': 'Fäktning är schemalagt',
    'archery': 'Bågskytte',
    'archeryScheduled': 'Bågskytte är schemalagt',
    'artsci': 'Konst och Vetenskap',
    'artsciScheduled': 'Aktiviteter för Konst och Vetenskap är schemalagt',
    'dancing': 'Dans',
    'dancingScheduled': 'Dans är schemalagt',
    'online': 'Sker online',
    'importantInfo': 'Viktig Info',
    'cancelled': 'Inställt',
    'cancelledLong': 'Detta event har blivit inställt',
    'contactStaff': 'Vänligen kontakta eventets autokrater för vidare information.',

    'bidList': 'Bud Lista',
    'addEvent': 'Lägg till ett event till kalendern',
    'pastEvents': 'Past Events',

    'hideLocal': 'Dölj lokala/icke-officiella event',
    'showLocal': 'Visa lokala/icke-officiella event',

    'hideCancelled': 'Dölj inställda event',
    'showCancelled': 'Visa inställda events',

    'eventsApprovedByChronicler': 'Event som har blivit godkända av Krönikören som officiella har en vit bakgrund.',
    'upcomingEvents': 'Kommande Event',

    // Columns of the calendar

    'date': 'Datum',
    'group': 'Grupp',
    'event': 'Event',
    'info': 'Info',

    // When loading, this shows "Laddar kalender försök 1", "Laddar kalender försök 2", and so on
    'loadingAttempt': 'Laddar kalender försök',
    'loadFailed': 'Kalender laddade ej upp',

    'eventNotFound': 'Detta event hittades inte i kalendern.',

    // Strings for the event pages

    'hostedBy': 'Arrangeras av',
    'viewMap': 'See på Google Maps',
    'siteAddress': 'Site adress',
    'siteInfo': 'Site information',
    'cost': 'Pris',
    'reservation': 'Bokning',
    'payment': 'Betalning',
    'joinOnline': 'Gå med i online mötet',
    'pwInfo': 'Lösenord information',
    'eventSteward': 'Huvudautokrat',
    'visitWebsite': 'Besök eventets websida',
    'visitFB': 'Följ detta event på Facebook',

    'chronOfficial': 'Det här evenemanget har godkänts för publicering av kungadömets krönikör.',
    'chronUpdated': 'Det här evenemanget har uppdaterats sedan det accepterades för publicering av krönikören.',
    'chronPending': 'Det här evenemanget inväntar granskning av krönikören.',
    'chronUnofficial': 'Kungadömets krönikör behöver mer information innan det här evenemanget kan godkännas för publicering.',
    'chronLocal': 'Det här lokala evenemanget har inte anmälts för publicering i kungadömets nyhetsbrev.',
    'chronOnline': 'Detta är ett enbart ett online-event.',

    // Info pages

    'appliesTo': 'Betrifft',
    'moreInfo': 'Mer information...',
    'informational': 'Informativt',

  },

  'fi': {
    'princeIdPresent': 'Insulae Draconisin ruhtinas läsnä',
    'princessIdPresent': 'Insulae Draconisin ruhtinatar läsnä ',
    'princeAndPrincessIdPresent': 'Insulae Draconisin ruhtinaspari läsnä',

    'princeNordmarkPresent': 'Nordmarkin ruhtinas läsnä',
    'princessNordmarkPresent': 'Nordmarkin ruhtinatar läsnä',
    'princeAndPrincessNordmarkPresent': 'Nordmarkin ruhtinaspari läsnä',

    'kingPresent': 'Kuningas läsnä',
    'queenPresent': 'Kuningatar läsnä',
    'kingAndQueenPresent': 'Kuningas ja kuningatar läsnä',
    'royaltyPresent': 'Kruunupäitä läsnä',

    'heavy': 'Rottinkitaistelua',
    'heavyScheduled': 'Rottinkitaistelua ohjelmassa',
    'fencing': 'Kalpamiekkailua',
    'fencingScheduled': 'Kalpamiekkailua ohjelmassa',
    'archery': 'Jousiammuntaa',
    'archeryScheduled': 'Jousiammuntaa ohjelmassa',
    'artsci': 'Tieteitä ja taiteita',
    'artsciScheduled': 'Tiede- ja taideaktiviteetteja ohjelmassa',
    'dancing': 'Tanssia',
    'dancingScheduled': 'Tanssia ohjelmassa',
    'online': 'Tapahtuu verkossa',
    'importantInfo': 'Tärkeää tietoa',
    'cancelled': 'Peruutettu',
    'cancelledLong': 'Tämä tapahtuma on peruutettu',
    'contactStaff': 'Ota yhteyttä tapahtumanjärjestäjiin lisätietoja varten.',

    'bidList': 'Lista järjestämistarjouksista',
    'addEvent': 'Lisää tapahtuma kalenteriin',
    'pastEvents': 'Past Events',

    'hideLocal': 'Piilota paikalliset/epäviralliset tapahtumat',
    'showLocal': 'Näytä paikalliset/epäviralliset tapahtumat',

    'hideCancelled': 'Piilota peruutetut tapahtumat',
    'showCancelled': 'Näytä peruutetut tapahtumat',

    'eventsApprovedByChronicler': 'Kronikoitsijan hyväksymät tapahtumat on merkitty valkoisella taustalla.',
    'upcomingEvents': 'Tulevat tapahtumat',

    // Columns of the calendar

    'date': 'Päivämäärä',
    'group': 'Ryhmä',
    'event': 'Tapahtuma',
    'info': 'Info',

    // When loading, this shows "Loading calendar attempt 1", "Loading calendar attempt 2", and so on
    'loadingAttempt': 'Ladataan kalenteria, yritys',
    'loadFailed': 'Kalenteria ei voitu ladata',

    'eventNotFound': 'Tätä tapahtumaa ei löytynyt kalenterista.',

    // Strings for the event pages

    'hostedBy': 'Järjestää',
    'viewMap': 'Katso sijainti Google Mapsista',
    'siteAddress': 'Tapahtumapaikan osoite',
    'siteInfo': 'Tietoa tapahtumapaikasta',
    'cost': 'Hinnasto',
    'reservation': 'Varaus',
    'payment': 'Maksu',
    'joinOnline': 'Liity verkkokoukseen',
    'pwInfo': 'Tietoa salasanasta',
    'eventSteward': 'Autokraatti',
    'visitWebsite': 'Siirry tapahtuman kotisivuille',
    'visitFB': 'Seuraa tätä tapahtumaa Facebookissa',

    'chronOfficial': 'Kuningaskunnan kronikoitsija on hyväksynyt tapahtuman julkaistavaksi',
    'chronUpdated': 'Kronikoitsija on päivittänyt tapahtumaa sen jälkeen, kun on se on hyväksytty julkaistavaksi',
    'chronPending': 'Tapahtuma odottaa kronikoitsijan tarkistusta',
    'chronUnofficial': 'Kuningaskunnan kronikoitsija tarvitsee lisää tietoja, ennen kuin tapahtuma voidaan hyväksyä julkaistavaksi',
    'chronLocal': 'Tätä paikallistapahtumaa ei julkaista kuningaskunnan tiedotteessa',
    'chronOnline': 'Tapahtuma ainoastaan verkossa.',

    // Info pages

    'appliesTo': 'Applies to',
    'moreInfo': 'More Information...',
    'informational': 'Informational',

  },

  'de': {
    'princeIdPresent': 'Prinz von Insulae Draconis anwesend',
    'princessIdPresent': 'Prinzessin von Insulae Draconis anwesend',
    'princeAndPrincessIdPresent': 'Prinzenpaar von Insulae Draconis anwesend',

    'princeNordmarkPresent': 'Prinz von Nordmark anwesend',
    'princessNordmarkPresent': 'Prinzessin  von Nordmark anwesend',
    'princeAndPrincessNordmarkPresent': 'Prinzenpaar von Nordmark anwesend',

    'kingPresent': 'König anwesend',
    'queenPresent': 'Königin anwesend',
    'kingAndQueenPresent': 'Königspaar anwesend',
    'royaltyPresent': 'Regenten anwesend',

    'heavy': 'Schwertkampf',
    'heavyScheduled': 'Schwertkampf ist geplant',
    'fencing': 'Fechten',
    'fencingScheduled': 'Fechten ist geplant',
    'archery': 'Bogenschießen',
    'archeryScheduled': 'Bogenschießen ist geplant',
    'artsci': 'Kunst & Wissenschaft (A&S)',
    'artsciScheduled': 'A&S Aktivitäten sind geplant',
    'dancing': 'Tanzen',
    'dancingScheduled': 'Tanzen ist geplant',
    'online': 'Findet online statt',
    'importantInfo': 'Wichtige Information',
    'cancelled': 'Abgesagt',
    'cancelledLong': 'Diese Veranstaltung wurde abgesagt.',
    'contactStaff': 'Bitte kontaktiere die Organisatoren für weitere Informationen.',

    'bidList': 'Orga-Angebote',
    'addEvent': 'Veranstaltung hinzufügen',
    'pastEvents': 'Past Events',

    'hideLocal': 'Blende lokale/inoffizielle Veranstaltungen aus.',
    'showLocal': 'Zeige lokale/inoffizielle Veranstaltungen an.',

    'hideCancelled': 'Blende  abgesagte Veranstaltungen aus.',
    'showCancelled': 'Zeige abgesagte Veranstaltungen an.',

    'eventsApprovedByChronicler': 'Veranstaltungen, die vom Chronisten offiziell anerkannt wurden, haben einen weißen Hintergrund.',
    'upcomingEvents': 'Nächste Veranstaltungen',

    // Columns of the calendar

    'date': 'Datum',
    'group': 'Gruppe',
    'event': 'Veranstaltung',
    'info': 'Information',

    // When loading, this shows "Loading calendar attempt 1", "Loading calendar attempt 2", and so on
    'loadingAttempt': 'Der Kalender wird geladen.',
    'loadFailed': 'Der Kalender konnte nicht geladen werden.',

    'eventNotFound': 'Die gesuchte Veranstaltung wurde im Kalender nicht gefunden.',

    // Strings for the event pages

    'hostedBy': 'Eingeladen von',
    'viewMap': 'Auf Google Maps anzeigen',
    'siteAddress': 'Adresse',
    'siteInfo': 'Veranstaltungsort',
    'cost': 'Kosten',
    'reservation': 'Reservierung',
    'payment': 'Zahlung',
    'joinOnline': 'Dem Onlinemeeting beitreten',
    'pwInfo': 'Passwortinformation',
    'eventSteward': 'Ansprechpartner',
    'visitWebsite': 'Webseite besuchen',
    'visitFB': 'Veranstaltung auf Facebook',

    'chronOfficial': 'Dieses Ereignis wurde vom Chronisten des Königreichs zur Veröffentlichung angenommen',
    'chronUpdated': 'Dieses Ereignis wurde seit der Annahme zur Veröffentlichung durch den Chronisten aktualisiert',
    'chronPending': 'Dieses Ereignis steht zur Überprüfung durch den Chronisten an',
    'chronUnofficial': 'Der Chronist des Königreichs benötigt weitere Informationen, bevor er dieses Ereignis zur Veröffentlichung annimmt',
    'chronLocal': 'Dieses lokale Ereignis wird nicht zur Veröffentlichung im Königreichs-Newsletter eingereicht',
    'chronOnline': 'Diese Veranstaltung findet nur online statt.',

    // Info pages

    'appliesTo': 'Betrifft to',
    'moreInfo': 'Mehr Informationen...',
    'informational': 'informationell'

  },

}

export default strings;